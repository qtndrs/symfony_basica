/*
    ./js/moreProjets.js
    JS et Ajax pour charger plus de projets
 */

    $(function(){

      $('.moreProjets').click(function(e){
        e.preventDefault();


        var nbreProjets = $('.projet-preview').length;
        var url = $(this).attr('data-url');

        $.ajax({
          url: url,
          data: {
            limit: 6,
            offset: nbreProjets
          },
          method: 'post',
          success: function(reponsePHP){
            $('.listeProjets').append(reponsePHP)
                            .find('.projet-preview:nth-last-of-type(-n+6)')
                            .hide().fadeIn();
          }
        });


      });

    });
