<?php
/*
    ./src/Controller/PostController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class PostController extends GenericController {

/**
 * [indexAction description]
 * @param  Request            $request   [description]
 * @param  PaginatorInterface $paginator [description]
 * @param  [type]             $limit     [description]
 * @param  string             $vue       [description]
 * @return array                        [description]
 */
public function indexAction(Request $request, PaginatorInterface $paginator, int $limit = null, string $vue = 'index'){
      $request = Request::createFromGlobals();
      $all = $this->_repository->findBy([], ["dateCreation" => "DESC"]);
      $posts = $paginator->paginate(
        $all,
        $request->query->getInt('page', 1),
        $limit
      );
      return $this->render('posts/'. $vue .'.html.twig',[
        'posts' => $posts
      ]);
    }
  }
