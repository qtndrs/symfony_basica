<?php
/*
    ./src/Controller/SlideController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Slide;
use Symfony\Component\HttpFoundation\Request;

class SlideController extends GenericController {

/**
 * [indexAction description]
 * @return array $slides [description]
 */
  public function indexAction(){
      $slides = $this->_repository->findAll();
      return $this->render('slides/index.html.twig',[
        'slides' => $slides
      ]);
    }
}
