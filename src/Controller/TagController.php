<?php
/*
    ./src/Controller/TagController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Tag;
use Symfony\Component\HttpFoundation\Request;

class TagController extends GenericController {

/**
 * [indexAction description]
 * @return array $tags [description]
 */
  public function indexAction(){
      $tags = $this->_repository->findAll();
      return $this->render('tags/index.html.twig',[
        'tags' => $tags
      ]);
    }
}
