<?php
/*
    ./src/Controller/ProjetController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Projet;
use Symfony\Component\HttpFoundation\Request;

class ProjetController extends GenericController {


/**
 * [indexAction description]
 * @param  string $vue     [description]
 * @param  array  $orderBy [description]
 * @param  int $limit   [description]
 * @return array $projets         [description]
 */
public function indexAction(string $vue = 'index', array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = null){
        $projets = $this->_repository->findBy([], $orderBy, $limit);
      return $this->render('projets/'.$vue.'.html.twig',[
        'projets' => $projets
      ]);
    }

/**
 * [moreAction description]
 * @param  string  $vue     [description]
 * @param  int  $limit   [description]
 * @param  Request $request [description]
 * @return array $projets           [description]
 */
public function moreAction(string $vue = 'more', int $limit = null, Request $request) {
        $offset = $request->request->get('offset');
        $projets = $this->_repository->findBy([], ["dateCreation" => "DESC"], $limit=6, $offset);
        return $this->render('projets/'.$vue.'.html.twig',[
          'projets' => $projets
        ]);
    }

/* public function findSimilarsProjetsAction(Projet $entity, string $route, string $vue = 'similar'){

  $sim=$this->getDoctrine()->getRepository(Projet::class)->findSimilarsProjets($entity->getTags());
        return $this->render('projets/'.$vue.'.html.twig',['entities'=>$sim,'route'=>$route]); */

}
