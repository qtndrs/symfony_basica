<?php
/*
    ./src/Controller/CategorieController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\Request;


class CategorieController extends GenericController {

/**
 * [indexAction description]
 * @return array [description]
 */
  public function indexAction(int $limit = null){
      $categories = $this->_repository->findAll($limit);
      return $this->render('categories/index.html.twig',[
        'categories' => $categories
      ]);
    }
}
