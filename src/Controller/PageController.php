<?php
/*
    ./src/Controller/PageController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Page;
use Symfony\Component\HttpFoundation\Request;

class PageController extends GenericController {

/**
 * [indexAction description]
 * @param  Request $request      [description]
 * @param  string  $current_path [description]
 * @return array                [description]
 */
  public function indexAction(Request $request, $current_path){
    $uri = explode('/', $_SERVER['REQUEST_URI']);
    $currentPage = isset($uri[7]) ? $uri[7] : '';

    $pages = $this->_repository->findAll();
    return $this->render('pages/index.html.twig',[
        'pages'=> $pages,
       'currentPage' => $currentPage,
       'current_path' => $current_path
    ]);

  }
}
