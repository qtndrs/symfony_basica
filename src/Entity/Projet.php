<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjetRepository")
 * @Vich\Uploadable
 */
class Projet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titreFR;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titreEN;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugFR;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugEN;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteLeadFR;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteLeadEN;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteSuiteFR;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteSuiteEN;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="projet_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $dateModification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="projets")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Auteur", inversedBy="projets")
     */
    private $auteur;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="projets")
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="projets")
     */
    private $categorie;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreFR(): ?string
    {
        return $this->titreFR;
    }

    public function setTitreFR(string $titreFR): self
    {
        $this->titreFR = $titreFR;

        return $this;
    }

    public function getTitreEN(): ?string
    {
        return $this->titreEN;
    }

    public function setTitreEN(string $titreEN): self
    {
        $this->titreEN = $titreEN;

        return $this;
    }

    public function getSlugFR(): ?string
    {
        return $this->slugFR;
    }

    public function setSlugFR(string $slugFR): self
    {
        $this->slugFR = $slugFR;

        return $this;
    }

    public function getSlugEN(): ?string
    {
        return $this->slugEN;
    }

    public function setSlugEN(string $slugEN): self
    {
        $this->slugEN = $slugEN;

        return $this;
    }

    public function getTexteLeadFR(): ?string
    {
        return $this->texteLeadFR;
    }

    public function setTexteLeadFR(?string $texteLeadFR): self
    {
        $this->texteLeadFR = $texteLeadFR;

        return $this;
    }

    public function getTexteLeadEN(): ?string
    {
        return $this->texteLeadEN;
    }

    public function setTexteLeadEN(?string $texteLeadEN): self
    {
        $this->texteLeadEN = $texteLeadEN;

        return $this;
    }

    public function getTexteSuiteFR(): ?string
    {
        return $this->texteSuiteFR;
    }

    public function setTexteSuiteFR(?string $texteSuiteFR): self
    {
        $this->texteSuiteFR = $texteSuiteFR;

        return $this;
    }

    public function getTexteSuiteEN(): ?string
    {
        return $this->texteSuiteEN;
    }

    public function setTexteSuiteEN(?string $texteSuiteEN): self
    {
        $this->texteSuiteEN = $texteSuiteEN;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'dateModification' is not defined in your entity, use another property
            $this->setdateModification = new \DateTime();
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getAuteur(): ?Auteur
    {
        return $this->auteur;
    }

    public function setAuteur(?Auteur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function __toString()
    {
    return $this->titreEN;
    }


    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Updated At
     *
     * @return \DateTime
     */
    public function getdateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set the value of Updated At
     *
     * @param \DateTime dateModification
     *
     * @return self
     */
    public function setdateModification(\DateTimeInterface $dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Set the value of Tags
     *
     * @param mixed tags
     *
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

}
