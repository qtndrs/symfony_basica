<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AuteurRepository")
 */
class Auteur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $biographieFR;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $biographieEN;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Projet", mappedBy="auteur")
     */
    private $projets;

    public function __construct()
    {
        $this->projets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getBiographieFR(): ?string
    {
        return $this->biographieFR;
    }

    public function setBiographieFR(?string $biographieFR): self
    {
        $this->biographieFR = $biographieFR;

        return $this;
    }

    public function getBiographieEN(): ?string
    {
        return $this->biographieEN;
    }

    public function setBiographieEN(?string $biographieEN): self
    {
        $this->biographieEN = $biographieEN;

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getProjets(): Collection
    {
        return $this->projets;
    }

    public function addProjet(Projet $projet): self
    {
        if (!$this->projets->contains($projet)) {
            $this->projets[] = $projet;
            $projet->setAuteur($this);
        }

        return $this;
    }

    public function removeProjet(Projet $projet): self
    {
        if ($this->projets->contains($projet)) {
            $this->projets->removeElement($projet);
            // set the owning side to null (unless already changed)
            if ($projet->getAuteur() === $this) {
                $projet->setAuteur(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
    return $this->nom;
    }
}
