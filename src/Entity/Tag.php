<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomFR;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomEN;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugFR;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugEN;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Projet", mappedBy="tags")
     */
    private $projets;

    public function __construct()
    {
        $this->projets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomFR(): ?string
    {
        return $this->nomFR;
    }

    public function setNomFR(string $nomFR): self
    {
        $this->nomFR = $nomFR;

        return $this;
    }

    public function getNomEN(): ?string
    {
        return $this->nomEN;
    }

    public function setNomEN(string $nomEN): self
    {
        $this->nomEN = $nomEN;

        return $this;
    }

    public function getSlugFR(): ?string
    {
        return $this->slugFR;
    }

    public function setSlugFR(string $slugFR): self
    {
        $this->slugFR = $slugFR;

        return $this;
    }

    public function getSlugEN(): ?string
    {
        return $this->slugEN;
    }

    public function setSlugEN(string $slugEN): self
    {
        $this->slugEN = $slugEN;

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getProjets(): Collection
    {
        return $this->projets;
    }

    public function addProjet(Projet $projet): self
    {
        if (!$this->projets->contains($projet)) {
            $this->projets[] = $projet;
            $projet->addTag($this);
        }

        return $this;
    }

    public function removeProjet(Projet $projet): self
    {
        if ($this->projets->contains($projet)) {
            $this->projets->removeElement($projet);
            $projet->removeTag($this);
        }

        return $this;
    }

    public function __toString()
    {
      return $this->nomEN;
    }

}
