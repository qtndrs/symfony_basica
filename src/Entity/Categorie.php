<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomFR;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomEN;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugFR;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugEN;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="categorie")
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Projet", mappedBy="categorie")
     */
    private $projets;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->projets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomFR(): ?string
    {
        return $this->nomFR;
    }

    public function setNomFR(string $nomFR): self
    {
        $this->nomFR = $nomFR;

        return $this;
    }

    public function getNomEN(): ?string
    {
        return $this->nomEN;
    }

    public function setNomEN(string $nomEN): self
    {
        $this->nomEN = $nomEN;

        return $this;
    }

    public function getSlugFR(): ?string
    {
        return $this->slugFR;
    }

    public function setSlugFR(string $slugFR): self
    {
        $this->slugFR = $slugFR;

        return $this;
    }

    public function getSlugEN(): ?string
    {
        return $this->slugEN;
    }

    public function setSlugEN(string $slugEN): self
    {
        $this->slugEN = $slugEN;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setCategorie($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getCategorie() === $this) {
                $post->setCategorie(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getProjets(): Collection
    {
        return $this->projets;
    }

    public function addProjet(Projet $projet): self
    {
        if (!$this->projets->contains($projet)) {
            $this->projets[] = $projet;
            $projet->setCategorie($this);
        }

        return $this;
    }

    public function removeProjet(Projet $projet): self
    {
        if ($this->projets->contains($projet)) {
            $this->projets->removeElement($projet);
            // set the owning side to null (unless already changed)
            if ($projet->getCategorie() === $this) {
                $projet->setCategorie(null);
            }
        }

        return $this;
    }

    public function __toString()
	{
		return $this->nomEN;
	}
}
