<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 */
class Page
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titreFR;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titreEN;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slugFR;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slugEN;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteFR;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteEN;

    /**
     * @ORM\Column(type="integer")
     */
    private $tri;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreFR(): ?string
    {
        return $this->titreFR;
    }

    public function setTitreFR(string $titreFR): self
    {
        $this->titreFR = $titreFR;

        return $this;
    }

    public function getTitreEN(): ?string
    {
        return $this->titreEN;
    }

    public function setTitreEN(string $titreEN): self
    {
        $this->titreEN = $titreEN;

        return $this;
    }

    public function getSlugFR(): ?string
    {
        return $this->slugFR;
    }

    public function setSlugFR(string $slugFR): self
    {
        $this->slugFR = $slugFR;

        return $this;
    }

    public function getSlugEN(): ?string
    {
        return $this->slugEN;
    }

    public function setSlugEN(string $slugEN): self
    {
        $this->slugEN = $slugEN;

        return $this;
    }

    public function getTexteFR(): ?string
    {
        return $this->texteFR;
    }

    public function setTexteFR(?string $texteFR): self
    {
        $this->texteFR = $texteFR;

        return $this;
    }

    public function getTexteEN(): ?string
    {
        return $this->texteEN;
    }

    public function setTexteEN(?string $texteEN): self
    {
        $this->texteEN = $texteEN;

        return $this;
    }

    public function getTri(): ?int
    {
        return $this->tri;
    }

    public function setTri(int $tri): self
    {
        $this->tri = $tri;

        return $this;
    }
}
