<?php

namespace App\Repository;

use App\Entity\Projet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Controller\PaginatorInterface;

/**
 * @method Projet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Projet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Projet[]    findAll()
 * @method Projet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Projet::class);
    }

/* public function findSimilarsProjets($array){

  $qb=$this->createQueryBuilder('p')
             ->select('p')
             ->leftJoin('p.tags','tags')
             ->addSelect('tags','p');
         if(is_iterable($array)){
            foreach ($array as $value) {
                $qb->andWhere(':val MEMBER OF p.tags')->setParameter('val', $value);
            }
        }else{
             $qb
             ->andWhere(':val MEMBER OF p.tags')->setParameter('val', $array);
         }
            return $qb->getQuery() ->getResult();
}*/


    /*
    public function findOneBySomeField($value): ?Projet
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
